using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Diagnostics;

namespace PikudDB
{
    public class clsDbGlobal
    {

        public static SqlConnection checkConn()
        {
            return makeConn();
        }

        public static SqlConnection makeConn()
        {
            SqlConnection myConnection = null;
            try
            {
                string connStr = ConfigurationManager.AppSettings["DB"];
                myConnection = new SqlConnection(connStr);
                myConnection.Open();
                return myConnection;
            }
            catch (Exception ex)
            {
                writeLog("error on makeConn=\n" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("error - make connection:" + ex.Message);
            }
        }


        public static Boolean closeConn(SqlConnection myConnection)
        {
            try
            {
                if (myConnection != null)
                    if (myConnection.State == ConnectionState.Open)
                    {
                        myConnection.Close();
                    }
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on closeConn=\n" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("error - close connection:" + ex.Message);
            }
        }

        public static DataTable getParamsByTableNum(int addEmpty, int paramTbl)
        {
            string strInParamsNames = "addEmpty,paramTbl";
            string strInParamsValues = addEmpty + "," + paramTbl;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsDbGlobal.GetDtFromSPNoReturnValue("[getParamsByTableNum]", arrInParamsNames, arrInParamsValues);
        }


        public static DataTable GetDtFromSP(string spName, string[] arrInParamsNames, string[] arrInParamsValues, string[] arrOutParamsNames, string[] arrOutParamsdbType,string[] arrOutParamsDbSize,string[] arrOutParamsValues)
        {
            SqlConnection myConnection = checkConn(); 
            try
            {
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }

                for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
                {
                    myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrOutParamsNames[idx], null));
                    myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Direction = ParameterDirection.Output;
                    if (arrOutParamsdbType[idx] == "int")
                    {
                        myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].DbType = DbType.Int32;
                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].DbType = DbType.String;
                    }
                    myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Size = Convert.ToInt32(arrOutParamsDbSize[idx]);
                }

                myCommand.Fill(dt);

                for (int idx = 0; idx < arrOutParamsNames.Length; idx++)
                {
                    arrOutParamsValues[idx] = myCommand.SelectCommand.Parameters["@" + arrOutParamsNames[idx]].Value.ToString();
                }
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSP=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDtFromSP=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }



        public static DataSet GetDsFromSPNoParamsNoReturnValue(string spName)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDsFromSPNoParamsNoReturnValue=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDsFromSPNoParamsNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static DataTable GetDtFromSPNoReturnValueIncTypes(string spName,
        string[] arrInParamsNames, string[] arrInParamsValues, SqlDbType[] arrInParamsTypes)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsTypes.Length > 0)
                    {
                        SqlParameter curPar = new SqlParameter("@" + arrInParamsNames[idx], arrInParamsTypes[idx]);
                        if (arrInParamsValues[idx].ToLower() == "null")
                        {
                            curPar.Value = DBNull.Value;
                        }
                        else
                        {
                            curPar.Value = arrInParamsValues[idx];
                        }
                        myCommand.SelectCommand.Parameters.Add(curPar);
                    }
                }
                myCommand.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSPNoReturnValue using sp=\n" +
                        spName + " - error=" + ex.Message +
                        "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDtFromSPNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }


        public static DataSet GetDsFromSPNoReturnValue(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            SqlConnection myConnection = checkConn(); 
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                myCommand.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDsFromSPNoReturnValue using sp=\n" +
                        spName + " - error=" + ex.Message +
                        "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDsFromSPNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static DataSet GetDsFromSPNoReturnValue(string spName, string[] arrInParamsNames, string[] arrInParamsValues
            , SqlDbType[] arrInParamsTypes)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsTypes.Length > 0)
                    {
                        SqlParameter curPar = new SqlParameter("@" + arrInParamsNames[idx], arrInParamsTypes[idx]);
                        if (arrInParamsValues[idx].ToLower() == "null")
                        {
                            curPar.Value = DBNull.Value;
                        }
                        else
                        {
                            curPar.Value = arrInParamsValues[idx];
                        }
                        myCommand.SelectCommand.Parameters.Add(curPar);
                    }
                }
                myCommand.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDsFromSPNoReturnValue using sp=\n" +
                        spName + " - error=" + ex.Message +
                        "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDsFromSPNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }



        public static DataTable GetDtFromSPNoReturnValue(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));

                    }
                    else
                    {
                        myCommand.SelectCommand.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                myCommand.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSPNoReturnValue using sp=\n" + 
                        spName + " - error=" + ex.Message + 
                        "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDtFromSPNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static void BatchBulkCopy(DataTable dataTable, string DestinationTbl, int batchSize)
        {
            // Get the DataTable 
            DataTable dtInsertRows = dataTable;
            SqlConnection myConnection = checkConn();
            var transaction = myConnection.BeginTransaction();
            using (SqlBulkCopy sbc = new SqlBulkCopy(myConnection, 
                SqlBulkCopyOptions.KeepIdentity,transaction))
            {
                sbc.DestinationTableName = DestinationTbl;

                // Number of records to be processed in one go
                sbc.BatchSize = batchSize;

                // Add your column mappings here
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    sbc.ColumnMappings.Add(dataTable.Columns[i].ColumnName, dataTable.Columns[i].ColumnName);                    
                }

                // Finally write to server
                sbc.WriteToServer(dtInsertRows);
                transaction.Commit();
            }
        }


        public static DataTable GetDtFromSPNoParamsNoReturnValue(string spName)
        {
            SqlConnection myConnection = checkConn(); 
            try
            {
                DataTable dt = new DataTable();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlDataAdapter myCommand = new SqlDataAdapter(spName, myConnection);
                myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                writeLog("error on GetDtFromSPNoParamsNoReturnValue=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("error on GetDtFromSPNoParamsNoReturnValue=\n" + spName + " - error=" + ex.Message);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static Boolean runSp_NoReturn(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                //    DataSet objDs = new DataSet();
                //ConfigurationManager.AppSettings["traceFolder"]
                SqlCommand cmd = new SqlCommand();
                int rowsAffected;
                cmd.CommandText = spName;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = myConnection;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                rowsAffected = cmd.ExecuteNonQuery();
                //myConnection.Close();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return false;
            }
        }

        public static Boolean runSp_NonQuery_NoParams_NoReturn(string spName)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_NonQuery_NoParams_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return false;
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static string runSp_ScalarQuery_NoParams_NoReturn(string spName)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
//                return cmd.ExecuteScalar().ToString();
                object myRet = cmd.ExecuteScalar();
                if (myRet != null)
                {
                    return myRet.ToString(); //ret.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_ScalarQuery_NoParams_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                return "err: " + ex.Message;
            }
            finally
            {
                closeConn(myConnection);
            }
        }


        public static string runSp_ScalarQuery_NoReturn(string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsValues[idx].ToLower() == "null")
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + arrInParamsNames[idx], arrInParamsValues[idx]));
                    }
                }
                object myRet = cmd.ExecuteScalar();
                if (myRet != null)
                {
                    return myRet.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_ScalarQuery_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("err=" + ex.Message + " - stack = " + ex.StackTrace + "\n -  runSp_ScalarQuery_NoReturn, sp=" +
                        spName);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        public static string runSp_ScalarQuery_NoReturn(string spName, string[] arrInParamsNames, string[] arrInParamsValues, SqlDbType[] arrInParamsTypes)
        {
            SqlConnection myConnection = checkConn();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = spName;
                cmd.Connection = myConnection;
                for (int idx = 0; idx < arrInParamsNames.Length; idx++)
                {
                    if (arrInParamsTypes.Length > 0)
                    {
                        SqlParameter curPar = new SqlParameter("@" + arrInParamsNames[idx], arrInParamsTypes[idx]);
                        if (arrInParamsValues[idx].ToLower() == "null")
                        {
                            curPar.Value = DBNull.Value;
                        }
                        else
                        {
                            curPar.Value = arrInParamsValues[idx];
                        }
                        cmd.Parameters.Add(curPar);
                    }
                }
                object myRet = cmd.ExecuteScalar();
                if (myRet != null)
                {
                    return myRet.ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                writeLog("error on runSp_ScalarQuery_NoReturn=\n" +
                        spName + " - error=" + ex.Message + "\n stack=" + ex.StackTrace, true);
                throw new Exception("err=" + ex.Message + " - stack = " + ex.StackTrace + "\n -  runSp_ScalarQuery_NoReturn, sp=" +
                        spName);
            }
            finally
            {
                closeConn(myConnection);
            }
        }

        #region helperFunction
        public static void writeLog(string msg, Boolean isErr)
        {
            string sSource = "PikudDB";
            //string sLog = "PikudDB";

            //if (!EventLog.SourceExists(sSource))
            //    EventLog.CreateEventSource(sSource, sLog);

            if (isErr)
            {
                EventLog.WriteEntry(sSource, " DB layer error found: \n\n" + msg
                                            , EventLogEntryType.Error,91);
            }
            else
            {
                EventLog.WriteEntry(sSource, " DB layer info msg: \n\n" + msg
                                            , EventLogEntryType.Information, 92);
            }
        }
        #endregion

    }
}
