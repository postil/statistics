﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PikudBL;
using System.Data;

namespace PostStatisticsMailerBL
{

   
    public class StatisticsBodyGenerator
    {
        DataSet FNDStatisticsFilteredData;
        string[] delimiter = clsUtil.arrDelimiter2;
        public string GetFilteredStatisticsReport(int statisticsTypeId, int statisticsCodeId, DateTime fromDate, DateTime toDate)
        {
            string htmlTable = string.Empty;
            FNDStatisticsFilteredData = GetFillteredData(statisticsTypeId, statisticsCodeId, fromDate, toDate);

           
            DataTable statisticsTypes = FNDStatisticsFilteredData.Tables[1];
            List<DataTableWrapper> DataTableWrapperList = new List<DataTableWrapper>(statisticsTypes.Rows.Count);

            DataTable dt = FNDStatisticsFilteredData.Tables[2];
            if(statisticsTypeId > 0)
            {
                string desc = statisticsTypes.Select("StatisticsTypeId=" + statisticsTypeId)[0]["statisticsTypeDescription"].ToString();
                DataTableWrapper typeOne = new DataTableWrapper(dt, true, desc);
                htmlTable += createTableBody(typeOne);
                return htmlTable;
            }
            foreach (DataRow statType in statisticsTypes.Rows)
            {
                DataTableWrapper cur;
                DataRow[] curStatFilter = dt.Select("StatisticsTypeId=" + statType["statisticsTypeId"].ToString());
                if (curStatFilter.Length == 0)
                {
                    cur = new DataTableWrapper(dt.Clone(), false, statType["statisticsTypeDescription"].ToString());
                }
                else
                {
                    cur = new DataTableWrapper(curStatFilter.CopyToDataTable(), true, statType["statisticsTypeDescription"].ToString());
                }
                DataTableWrapperList.Add(cur);
            }

            foreach (DataTableWrapper dataWrapper in DataTableWrapperList)
            {
                htmlTable += createTableBody(dataWrapper);
            }
            return htmlTable;
        }

        private DataSet GetFillteredData(int statisticsTypeId, int statisticsCodeId, DateTime fromDate, DateTime toDate)
        {
            string strInParamsNames = "StatisticsTypeId" + delimiter[0] + "StatisticsCodeId" + delimiter[0] + "fromDate" + delimiter[0] + "toDate";
           
            string strInParamsValues = statisticsTypeId + delimiter[0] + statisticsCodeId + delimiter[0] + fromDate + delimiter[0] + toDate;
            string[] arrInParamsNames = strInParamsNames.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            string[] arrInParamsValues = strInParamsValues.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
            SqlDbType[] arrInParamsTypes = new SqlDbType[4];
            arrInParamsTypes[0] = SqlDbType.Int;
            arrInParamsTypes[1] = SqlDbType.Int;
            arrInParamsTypes[2] = SqlDbType.DateTime;
            arrInParamsTypes[3] = SqlDbType.DateTime;
            return clsUtil.FillDS("dbo.StatisticsGetFilteredData", arrInParamsNames, arrInParamsValues, arrInParamsTypes);
        }

        /*
       * create the body of the mail from the data received
       * param - sendingParameterCode - the sending paramater - daily, weekly or monthly
       * dt - the datatable with the data         
       */
        private static string createTableBody(DataTableWrapper dataWrapper)
        {
            DataTable tableColumn;
            DataTable datesTable;
            StringBuilder sb = new StringBuilder();
            if (dataWrapper != null && dataWrapper.HasRows)
            {
                DataTable data = dataWrapper.Data;
                sb.Append("<table class='tbl'>");
                string curDate = string.Empty;
                string header = CreateTableHeader(data, out tableColumn, out datesTable, dataWrapper.StatisticsTypeDescription);
                sb.Append(header);
                bool alternateRow = false;
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    if (i % tableColumn.Rows.Count == 0)
                    {

                        if (alternateRow)
                        {
                            sb.Append("<tr class='trGrey'>");
                        }
                        else
                        {
                            sb.Append("<tr>");
                        }
                        alternateRow = !alternateRow;
                        sb.Append("<td class='tdStyle'>");
                        sb.Append(DateTime.Parse(data.Rows[i]["date"].ToString()).ToString("dd/MM/yyyy"));
                        sb.Append("</td>");
                    }
                    sb.Append("<td class='tdStyle'>");
                    sb.Append(data.Rows[i]["Quantity"] != DBNull.Value ? string.Format("{0:n0}", int.Parse(data.Rows[i]["Quantity"].ToString())) : "No results");
                    sb.Append("</td>");
                    if (i % tableColumn.Rows.Count == tableColumn.Rows.Count - 1) //last td in a row 
                    {
                        sb.Append("</tr>");
                    }

                }
                sb.Append("</table>");
            }
            else
            {
                sb.Append("<table>");
                sb.AppendLine("<tr><th>סטטיסטיקות " + dataWrapper.StatisticsTypeDescription + "</th></tr>");
                sb.AppendLine("<tr><td>אין רשומות</td></tr>");
                sb.Append("<table>");
            }

            return sb.ToString();
        }

        private static string CreateTableHeader(DataTable data, out DataTable tableColumn, out DataTable datesTable, string typeDescription)
        {
            string res = string.Empty;
            tableColumn = new DataTable();
            datesTable = new DataTable();
            try
            {
                //get all the headers to see if we addesd some code during the week or month                         
                tableColumn = data.DefaultView.ToTable(true, new string[] { "StatisticsCodeId", "StatisticsCodesDescription" });
                //get all the dates in the table 
                datesTable = data.DefaultView.ToTable(true, new string[] { "date" });
                string colspan = (tableColumn.Rows.Count + 1).ToString();
                StringBuilder sb = new StringBuilder("<tr><th colspan='" + colspan + "'>סטטיסטיקות " + typeDescription + "</th></tr><tr><th class='thStyle'>תאריך</th>");
                for (int i = 0; i < tableColumn.Rows.Count; i++)
                {
                    //set the text of each cell with the statistics code description
                    //e.g. "סה"כ מבנים תקינים עם נקודות מסירה בנח"
                    sb.Append("<th class='thStyle'>" + tableColumn.Rows[i]["StatisticsCodesDescription"].ToString() + "</th>");
                }
                sb.Append("</tr>");
                res = sb.ToString();
            }
            catch (Exception ex)
            {
                res = string.Empty;
               
            }
            return res;


        }
    }

    class DataTableWrapper
    {
        DataTable _data;

        public DataTable Data
        {
            get { return _data; }
            set { _data = value; }
        }
        bool _hasRows;

        public bool HasRows
        {
            get { return _hasRows; }
            set { _hasRows = value; }
        }
        string _statisticsTypeDescription;

        public string StatisticsTypeDescription
        {
            get { return _statisticsTypeDescription; }
            set { _statisticsTypeDescription = value; }
        }

        public DataTableWrapper(DataTable data, bool hasRows, string typeDescription)
        {
            _data = data;
            _hasRows = hasRows;
            _statisticsTypeDescription = typeDescription;
        }

    }

}
