﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Data;
using PikudBL;


namespace PostStatisticesMailer
{
    class Program
    {

        static DataSet FNDStatisticsForGroupsAndData;


        static void Main(string[] args)
        {
            writeLog("start *******************************************************************");
            //Insert todays data
            try
            {
                writeLog("Insert todays data *******************************************************************");
                bool res = clsUtil.ExecuteSQLNoParams("dbo.StatisticsInsertDailyData");
                if (res)
                {
                    //get dataset of 
                    //1. groups and send parameter
                    //2. all statistics codes
                    //3. all statistics types
                    //4. data of current day
                    //5. data of last 7 days
                    //6. data of last month
                    FNDStatisticsForGroupsAndData = clsUtil.FillDS("dbo.StatisticsGetData");
                    SendMailByParameters(FNDStatisticsForGroupsAndData);
                }

                
            }
            catch (Exception ex)
            {
                writeLog("general exception,  exception details=" + ex.Message + " - stack=" + ex.StackTrace);
            }
            finally
            {
                writeLog("Finish *******************************************************************");
            }
            
        }

   

        private static void SendMailByParameters(DataSet FNDStatisticsForGroupsAndData)
        {
            writeLog("SendMailByParameters *******************************************************************");
            int SendingParameterCode = 0;
            string mailAddress = string.Empty;            
            bool isDebug = bool.Parse(ConfigurationManager.AppSettings["isDebug"]);
            //create the mail tables style
            string mailStyle = createStyleForHTML();
            //string to store the bodies of the types of mails
            string strBodyDailyMail, strBodyWeeklyMail, strBodyMonthlyMail;
            //get the day of month for the monthly check
            int dayOfMonth = 1;             
            //get the weekday for the weekly check
            string dayOfWeek = "Sunday";

            if (isDebug)
            {//if we are in debug mode use the parameters from app config for day of month and day of week
                //else day od week = Sunday and day of month is the 1st
                dayOfMonth = int.Parse(ConfigurationManager.AppSettings["dayOfMonth"].ToString());
                dayOfWeek = Enum.GetName(typeof(DayOfWeek), int.Parse(ConfigurationManager.AppSettings["dayOfWeek"].ToString()) - 1);
            }           
            //create the mail one time for duture use;
            if (FNDStatisticsForGroupsAndData != null && FNDStatisticsForGroupsAndData.Tables.Count > 0)
            {

                DataTable groupsSettings = FNDStatisticsForGroupsAndData.Tables[0];
                DataTable statisticsTypes = FNDStatisticsForGroupsAndData.Tables[2];
                //create the mail body for the daily mail
                strBodyDailyMail = CreateHTMLBodyFromDataTable(1, FNDStatisticsForGroupsAndData.Tables[3], statisticsTypes);
                //create the mail body for the weekly mail
                strBodyWeeklyMail = CreateHTMLBodyFromDataTable(2, FNDStatisticsForGroupsAndData.Tables[4], statisticsTypes);
                //create the mail body for the monthly mail
                strBodyMonthlyMail = CreateHTMLBodyFromDataTable(3, FNDStatisticsForGroupsAndData.Tables[5], statisticsTypes);

                //sets the message setting for sending the mails using the right mail body
                MailMessage dailyMessage = SetMessage(strBodyDailyMail, 1);
                MailMessage weeklyMessage = SetMessage(strBodyWeeklyMail, 2);
                MailMessage montlyMessage = SetMessage(strBodyMonthlyMail, 3);
                SmtpClient client = new SmtpClient();
                if (groupsSettings != null && groupsSettings.Rows.Count > 0)
                {
                    foreach (DataRow dr in groupsSettings.Rows)
                    { //for each row (that includes the SendingParameterCode and email, add the email to the To collection 
                        if (dr["email"] != DBNull.Value && !string.IsNullOrEmpty(dr["email"].ToString()))
                        {//has mail                            
                            SendingParameterCode = int.Parse(dr["SendingParameterCode"].ToString());
                            if (SendingParameterCode == 1)
                            {//daily mail
                                dailyMessage.To.Add(dr["email"].ToString());

                            }
                            else if (SendingParameterCode == 2 && DateTime.Now.DayOfWeek.ToString() == dayOfWeek)
                            {//weekly mail and the right day of week
                                weeklyMessage.To.Add(dr["email"].ToString());

                            }
                            else if (SendingParameterCode == 3 && DateTime.Now.Date.Day == dayOfMonth)
                            {//monthly mail and the right day of month
                                montlyMessage.To.Add(dr["email"].ToString());
                            }
                        }
                    }
                    if (dailyMessage.To.Count > 0)
                    {
                        writeLog("Send daily mail *******************************************************************");
                        client.Send(dailyMessage);
                    }
                    if (weeklyMessage.To.Count > 0)
                    {
                        writeLog("Send weekly mail *******************************************************************");
                        client.Send(weeklyMessage);
                    }
                    if (montlyMessage.To.Count > 0)
                    {
                        writeLog("Send monthly mail *******************************************************************");
                        client.Send(montlyMessage);
                    }
                }
                //send all the mails with the right body and recipients

            }
        }


        /*
         * function to build the mails tables with no results in them
         * Not in use
         * /

        private static void SendMailByParametersWithNoResults(DataSet FNDStatisticsForGroupsAndData)
        {
            writeLog("SendMailByParameters *******************************************************************");
            int SendingParameterCode = 0;
            string mailAddress = string.Empty;
            //create the mail tables style
            string mailStyle = createStyleForHTML();
            //string to store the bodies of the types of mails
            string strBodyDailyMail, strBodyWeeklyMail, strBodyMonthlyMail;
            //get the day of month for the monthly check
            int dayOfMonth = int.Parse(ConfigurationManager.AppSettings["dayOfMonth"].ToString());
            //get the weekday for the weekly check
            string dayOfWeek = Enum.GetName(typeof(DayOfWeek), int.Parse(ConfigurationManager.AppSettings["dayOfWeek"].ToString()));
            //create the mail one time for duture use;

            if (FNDStatisticsForGroupsAndData != null && FNDStatisticsForGroupsAndData.Tables.Count > 0)
            {

                DataTable groupsSettings = FNDStatisticsForGroupsAndData.Tables[0];
                DataTable statisticsTypes = FNDStatisticsForGroupsAndData.Tables[2];
                //create the mail body for the daily mail
                //create the mail body for the daily mail
                strBodyDailyMail = CreateHTMLBodyFromDataTable(1, FNDStatisticsForGroupsAndData.Tables[3], statisticsTypes);
                //create the mail body for the weekly mail
                strBodyWeeklyMail = CreateHTMLBodyFromDataTable(2, FNDStatisticsForGroupsAndData.Tables[4], statisticsTypes);
                //create the mail body for the monthly mail
                strBodyMonthlyMail = CreateHTMLBodyFromDataTable(3, FNDStatisticsForGroupsAndData.Tables[5], statisticsTypes);

                //sets the message setting for sending the mails using the right mail body
                MailMessage dailyMessage = SetMessage(strBodyDailyMail, 1);
                MailMessage weeklyMessage = SetMessage(strBodyWeeklyMail, 2);
                MailMessage montlyMessage = SetMessage(strBodyMonthlyMail, 3);
                SmtpClient client = new SmtpClient();
                if (groupsSettings != null && groupsSettings.Rows.Count > 0)
                {                    
                    foreach (DataRow dr in groupsSettings.Rows)
                    { //for each row (that includes the SendingParameterCode and email, add the email to the To collection 
                        if (dr["email"] != DBNull.Value && !string.IsNullOrEmpty(dr["email"].ToString()))
                        {//has mail                            
                            SendingParameterCode = int.Parse(dr["SendingParameterCode"].ToString());
                            if (SendingParameterCode == 1)
                            {//daily mail
                                dailyMessage.To.Add(dr["email"].ToString());

                            }
                            else if (SendingParameterCode == 2 && DateTime.Now.DayOfWeek.ToString() == dayOfWeek)
                            {//weekly mail and the right day of week
                                weeklyMessage.To.Add(dr["email"].ToString());

                            }
                            else if (SendingParameterCode == 3 && DateTime.Now.Date.Day == dayOfMonth)
                            {//monthly mail and the right day of month
                                montlyMessage.To.Add(dr["email"].ToString());
                            }
                        }
                    }
                    if (dailyMessage.To.Count > 0)
                    {
                        writeLog("Send daily mail *******************************************************************");
                        client.Send(dailyMessage);
                    }
                    if (weeklyMessage.To.Count > 0)
                    {
                        writeLog("Send weekly mail *******************************************************************");
                        client.Send(weeklyMessage);
                    }
                    if (montlyMessage.To.Count > 0)
                    {
                        writeLog("Send monthly mail *******************************************************************");
                        client.Send(montlyMessage);
                    }
                }
                //send all the mails with the right body and recipients

            }
        }

        /*
         * create mail message with default setting for html sending
         * param strBody - the mail's body 
         */
        private static MailMessage SetMessage(string strBody, int sendingParameter)
        {
            MailMessage res = new MailMessage();
            res.Subject = GetSubjetBySendingParameter(sendingParameter);
            res.SubjectEncoding = Encoding.UTF8;
            res.IsBodyHtml = true;
            res.BodyEncoding = Encoding.UTF8;
            res.Body = "<HTML><head><meta http-equiv='Content-Type'  content='text/html charset=UTF-8' />" +
                      createStyleForHTML() +
                              "</head><BODY><div style='direction:rtl;text-align:right'>" + strBody + "</div></BODY></HTML>";

            return res;
        }

        private static string GetSubjetBySendingParameter(int sendingParameter)
        {
            string res = "פירוט סטטיסטיקות ";
            switch (sendingParameter)
            {
                case 1:
                    res += "יומי";
                    break;
                case 2:
                    res += "שבועי";
                    break;
                default:
                    res += "חודשי";
                    break;
            }
            return res;
        }

        /*
         * create the mail tables style
         */
        private static string createStyleForHTML()
        {
            StringBuilder sb = new StringBuilder("<style>");
            sb.Append(".mainDiv{margin:10px;} ");
            sb.Append(".tbl{border:1px solid black;border-spacing:0px;text-align:center;} ");
            sb.Append(".tdStyle{padding: 0px;border-left:1px black solid;} ");
            sb.Append(".thStyle{width:100px;background-color:#BBDEFB;padding: 0px;border-left:1px black solid;} ");
            sb.Append(".boldSpan{font-weight:bold;} ");
            sb.Append(".trGrey{background-color:#C0C0C0;} ");
            
            
            sb.Append("</style>");
            return sb.ToString();
        }

        /*
         * send the mail using the datatable
         * param - sendingParameterCode - the sending paramater - daily, weekly or monthly
         * param - dt - the datatable with the data
         * param - emailAddress - the address to send the mail to
         * CURRENTLY NOT IN USE
         
        private static void SendStatisticMail(int sendingParameterCode, string emailAddress, DataTable data)
        {
            try
            {
                string strBody = string.Empty;
                string strTo = emailAddress;
                string strSubject = string.Empty;
                MailMessage message = new MailMessage();
                strBody = CreateHTMLBodyFromDataTable(sendingParameterCode, data);
                message.To.Add(strTo);
                message.Subject = strSubject;
                message.SubjectEncoding = Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Body = "<HTML><head><meta http-equiv='Content-Type'  content='text/html charset=UTF-8' />" +
                        createStyleForHTML() +
                                "</head><BODY><div style='direction:rtl;text-align:right'>" + strBody + "</div></BODY></HTML>";
                message.BodyEncoding = Encoding.UTF8;

                SmtpClient client = new SmtpClient();


                client.Send(message);
            }
            catch (Exception ex)
            {
                //writeLog("trying to send email to=" + strTo + ",  send email failed,   exception details=" + ex.Message + " - stack=" + ex.StackTrace);
            }
        }
        /*
         
        /*
         * create the body of the mail from the data received         
         * param - sendingParameterCode - the sending paramater - daily, weekly or monthly
         * param - dt - the datatable with the data
         */
        private static string CreateHTMLBodyFromDataTable(int sendingParameterCode, DataTable dt, DataTable statisticsTypes)
        {
            // Split into tables by each employee
            string res = string.Empty;
            //create list of datatables for each statistic type 
            //from each table we create the right mail;
            #region linq not in use
            //List<DataTable> dataTabels = dt.AsEnumerable()
            //                        .GroupBy(row => row.Field<int>("StatisticsTypeId"))
            //                        .Select(g => g.CopyToDataTable())
            //                        .ToList();
            #endregion

            //split the data into all datatables for each statistics type
            List<DataTableWrapper> DataTableWrapperList = new List<DataTableWrapper>(statisticsTypes.Rows.Count);
            foreach(DataRow statType in statisticsTypes.Rows)
            {
                DataTableWrapper cur;
                DataRow[] curStatFilter = dt.Select("StatisticsTypeId=" + statType["statisticsTypeId"].ToString());
                if (curStatFilter.Length == 0)
                {
                    cur = new DataTableWrapper(dt.Clone(), false, statType["statisticsTypeDescription"].ToString());
                }
                else
                {
                    cur = new DataTableWrapper(curStatFilter.CopyToDataTable(), true, statType["statisticsTypeDescription"].ToString());                    
                }
                DataTableWrapperList.Add(cur);
            }

            foreach (DataTableWrapper dataWrapper in DataTableWrapperList)
            {
                res += createTableBody(sendingParameterCode, dataWrapper);
            }
            return res;
        }


        /*
         * create the body of the mail from the data received
         * param - sendingParameterCode - the sending paramater - daily, weekly or monthly
         * dt - the datatable with the data         
         */
        private static string createTableBody(int sendingParameterCode, DataTableWrapper dataWrapper)
        {

            StringBuilder sb = new StringBuilder("<div class='mainDiv'><span class='boldSpan'>סטטיסטיקות " + dataWrapper.StatisticsTypeDescription + " ");
            string timeText = string.Empty;
            DataTable tableColumn;
            DataTable datesTable;
            switch (sendingParameterCode)
            {
                case 2: //last 7 days (not including today)
                    timeText = "לשבוע אחרון";
                    break;
                case 3: //last month
                    timeText = "לחודש אחרון";
                    break;
                default: //yesterday
                    timeText = "ליום אחרון";
                    break;

            }
            sb.Append(timeText);
            sb.Append("</span></div>");

            if (dataWrapper != null && dataWrapper.HasRows)
            {
                DataTable data = dataWrapper.Data;
                sb.Append("<table class='tbl'>");
                string curDate = string.Empty;
                string header = CreateTableHeader(data, out tableColumn, out datesTable);
                sb.Append(header);
                bool alternateRow = false;
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    if (i % tableColumn.Rows.Count == 0)
                    {
                        
                        if (alternateRow)
                        {
                            sb.Append("<tr class='trGrey'>");
                        }
                        else
                        {
                            sb.Append("<tr>");
                        }
                        alternateRow = !alternateRow;
                        sb.Append("<td class='tdStyle'>");
                        sb.Append(DateTime.Parse(data.Rows[i]["date"].ToString()).ToString("dd/MM/yyyy"));
                        sb.Append("</td>");
                    }
                    sb.Append("<td class='tdStyle'>");
                    sb.Append(data.Rows[i]["Quantity"] != DBNull.Value ? string.Format("{0:n0}", int.Parse(data.Rows[i]["Quantity"].ToString())) : "No results");
                    sb.Append("</td>");
                    if (i % tableColumn.Rows.Count == tableColumn.Rows.Count - 1) //last td in a row 
                    {
                        sb.Append("</tr>");
                    }

                }
                sb.Append("</table>");
            }
            else
            {
                sb.Append("אין רשומות");
            }
           
            return sb.ToString();
        }

        /*
        * create the body of the mail from the data received
        * when there is a missing value for a given day and statistics code paramater
        * the table cell will show the text "no results"
        * this kind of behaviour can be reached when adding or removing statistics code
        * and sending weekly or monthly mail
        * param - sendingParameterCode - the sending paramater - daily, weekly or monthly
        * dt - the datatable with the data
         * CURRENTLY NOT IN USE
       
        private static string createTableBodyWithEmpty(int sendingParameterCode, DataTable data)
        {

            string res = string.Empty;
            try
            {
                //the header for the current statistics we are referencing
                //  e.g : "ממשק נוח", "ממשק בדיקות
                StringBuilder sb = new StringBuilder("<div class='mainDiv'>סטטיסטיקות " + data.Rows[0]["StatisticsTypeDescription"].ToString() + " ");
                string timeText = string.Empty;
                DataTable tableColumn;
                DataTable datesTable;
                switch (sendingParameterCode)
                {
                    case 2: //last 7 days (not including today)
                        timeText = "לשבוע אחרון";
                        break;
                    case 3: //last month
                        timeText = "לחודש אחרון";
                        break;
                    default: //yesterday
                        timeText = "ליום אחרון";
                        break;

                }
                sb.Append(timeText);
                sb.Append("</div>");
                sb.Append("<table class='tbl'>");
                if (data != null && data.Rows.Count > 0)
                {
                    string curDate = string.Empty;
                    //create the header of the html table
                    //get back the tablecolumn for the linq query
                    //get the dates for the rows of the table
                    string header = CreateTableHeader(data, out tableColumn, out datesTable);
                    sb.Append(header);
                    for (int i = 0; i < datesTable.Rows.Count; i++)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class='tdStyle'>");
                        sb.Append(DateTime.Parse(datesTable.Rows[i]["date"].ToString()).ToString("dd/MM/yyyy"));
                        sb.Append("</td>");
                        for (int j = 0; j < tableColumn.Rows.Count; j++)
                        {
                            //find the right quantity if there is one, if not insert empty td
                            //we need to check whether for the given date there is data for the 
                            //given statistics code
                            DataRow quantityRow = data.AsEnumerable()
                                .Where(row => row.Field<DateTime>("date") == DateTime.Parse(datesTable.Rows[i]["date"].ToString())
                                && row.Field<int>("StatisticsCodeId") == int.Parse(tableColumn.Rows[j]["StatisticsCodeId"].ToString())).SingleOrDefault();
                            //&& row.Field<int>("StatisticsCodeId") == int.Parse(datesTable.Rows[i]["StatisticsCodeId"].ToString())));
                            if (quantityRow == null)
                            {// no data for the given statistics code and quantity
                                sb.Append("<td class='tdStyle'>");
                                sb.Append("no results");
                                sb.Append("</td>");
                            }
                            else
                            {//data for the given statistics code and quantity
                                sb.Append("<td class='tdStyle'>");
                                sb.Append(quantityRow["quantity"].ToString());
                                sb.Append("</td>");
                            }
                        }
                    }
                    sb.Append("</tr>");
                }


                sb.Append("</table>");
                res = sb.ToString();
            }
            catch (Exception ex)
            {
                writeLog("error when trying to create dates table createTableBodyWithEmpty, exception details=" + ex.Message + " - stack=" + ex.StackTrace);
                res = string.Empty;
            }
            return res;
        }
         */
        /*
         * creates the header cells of the html table from the 
         * param data - the datatable with the rows of information
         * param out tableColumn - a table that will hold the columns names of the table for use
         * when inserting the table body cells 
         * param datesTable - a table that will hold the dates for the mail
         */
        private static string CreateTableHeader(DataTable data, out DataTable tableColumn, out DataTable datesTable)
        {
            string res = string.Empty;
            tableColumn = new DataTable();
            datesTable = new DataTable();
            try
            {
                //get all the headers to see if we addesd some code during the week or month                         
                tableColumn = data.DefaultView.ToTable(true, new string[] { "StatisticsCodeId", "StatisticsCodesDescription" });
                //get all the dates in the table 
                datesTable = data.DefaultView.ToTable(true, new string[] { "date" }); 
                StringBuilder sb = new StringBuilder("<tr><th class='thStyle'>תאריך</th>");
                for (int i = 0; i < tableColumn.Rows.Count; i++)
                {
                    //set the text of each cell with the statistics code description
                    //e.g. "סה"כ מבנים תקינים עם נקודות מסירה בנח"
                    sb.Append("<th class='thStyle'>" + tableColumn.Rows[i]["StatisticsCodesDescription"].ToString() + "</th>");
                }
                sb.Append("</tr>");
                res = sb.ToString();
            }
            catch (Exception ex)
            {
                res = string.Empty;
                writeLog("error when trying to create dates table CreateTableHeader, exception details=" + ex.Message + " - stack=" + ex.StackTrace);
            }
            return res;


        }

        /*
         * writes a row in the log file for given action:
         * start, finish, error, sending daily mail, sending weekly mail, sending monthly mail
         */
        private static void writeLog(string msg)
        {
            StreamWriter sw = new StreamWriter("Log.txt", true, UTF8Encoding.UTF8);
            sw.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "  -  " + msg + "\n");
            sw.Flush();
            sw.Close();
            
        }

    }

    class DataTableWrapper
    {
        DataTable _data;

        public DataTable Data
        {
            get { return _data; }
            set { _data = value; }
        }
        bool _hasRows;

        public bool HasRows
        {
            get { return _hasRows; }
            set { _hasRows = value; }
        }
        string _statisticsTypeDescription;

        public string StatisticsTypeDescription
        {
            get { return _statisticsTypeDescription; }
            set { _statisticsTypeDescription = value; }
        }

        public DataTableWrapper(DataTable data, bool hasRows, string typeDescription)
        {
            _data = data;
            _hasRows = hasRows;
            _statisticsTypeDescription = typeDescription;
        }

    }


}

