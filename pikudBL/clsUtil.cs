using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using PikudDB;
using System.Data;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;
using System.Web;

namespace PikudBL
{
    public class clsUtil
    {

        static string strRegx = @"^[0-9a-zA-Z" + //any letter or number
                            "�-�" + // heb letters
                            @"!@#$%^&*_:;,.=+<>?\s'" + //some special chars and space
                            @"""" + //double quotes
                            @"\-\(\)\{\}\[\]/\\" + //some reserved chars that not allowed without slash
                            @"]*$"; //close the regx

         static string strPassPharse = ConfigurationManager.AppSettings["passPhrase"];
         static string strSalt = ConfigurationManager.AppSettings["salt"];
        static string strVector = ConfigurationManager.AppSettings["vector"];
         static string hashAlgorithm = "SHA1";
         static int passwordIterations = 2;
         static int keySize = 128;
         public static string[] arrDelimiter = new string[1] { "!$!" };
         public static string[] arrCommaDelimiter = new string[1] { "," };
         public static string[] arrDelimiter2 = new string[1] { "!@!" };




//******************************************start encrypt***********************************************
#region summary of encrypt params
        /// <summary>
        /// Encrypts specified plaintext using Rijndael symmetric key algorithm
        /// and returns a base64-encoded result.
        /// </summary>
        /// <param name="plainText">
        /// Plaintext value to be encrypted.
        /// </param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be 
        /// exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        /// Longer keys are more secure than shorter keys.
        /// </param>
        /// <returns>
        /// Encrypted value formatted as a base64-encoded string.
        /// </returns>
#endregion summary of encrypt params
        public static string Encrypt(string plainText)
        {
            // Convert strings into byte arrays.
            // Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            // encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(strVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(strSalt);

            // Convert our plaintext into a byte array.
            // Let us assume that plaintext contains UTF8-encoded characters.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // First, we must create a password, from which the key will be derived.
            // This password will be generated from the specified passphrase and 
            // salt value. The password will be created using the specified hash 
            // algorithm. Password creation can be done in several iterations.
            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            strPassPharse,
                                                            saltValueBytes,
                                                            hashAlgorithm,
                                                            passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                         encryptor,
                                                         CryptoStreamMode.Write);
            // Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText;
        }

        /// <summary>
        /// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        /// </summary>
        /// <param name="cipherText">
        /// Base64-formatted ciphertext value.
        /// </param>
        /// <param name="passPhrase">
        /// Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key.
        /// Passphrase can be any string. In this example we assume that this
        /// passphrase is an ASCII string.
        /// </param>
        /// <param name="saltValue">
        /// Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.
        /// </param>
        /// <param name="hashAlgorithm">
        /// Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        /// </param>
        /// <param name="passwordIterations">
        /// Number of iterations used to generate password. One or two iterations
        /// should be enough.
        /// </param>
        /// <param name="initVector">
        /// Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be
        /// exactly 16 ASCII characters long.
        /// </param>
        /// <param name="keySize">
        /// Size of encryption key in bits. Allowed values are: 128, 192, and 256.
        /// Longer keys are more secure than shorter keys.
        /// </param>
        /// <returns>
        /// Decrypted string value.
        /// </returns>
        /// <remarks>
        /// Most of the logic in this function is similar to the Encrypt
        /// logic. In order for decryption to work, all parameters of this function
        /// - except cipherText value - must match the corresponding parameters of
        /// the Encrypt function which was called to generate the
        /// ciphertext.
        /// </remarks>
        public static string Decrypt(string cipherText)
        {
            // Convert strings defining encryption key characteristics into byte
            // arrays. Let us assume that strings only contain ASCII codes.
            // If strings include Unicode characters, use Unicode, UTF7, or UTF8
            // encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(strVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(strSalt);

            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            // First, we must create a password, from which the key will be 
            // derived. This password will be generated from the specified 
            // passphrase and salt value. The password will be created using
            // the specified hash algorithm. Password creation can be done in
            // several iterations.
            PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                            strPassPharse,
                                                            saltValueBytes,
                                                            hashAlgorithm,
                                                            passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption
            // key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();

            // It is reasonable to set encryption mode to Cipher Block Chaining
            // (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC;

            // Generate decryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key 
            // bytes.
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                             keyBytes,
                                                             initVectorBytes);

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                          decryptor,
                                                          CryptoStreamMode.Read);

            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);

            // Return decrypted string.   
            return plainText;
        }
        //******************************************end encrypt******************************************
        public static DataTable FillDt
                    (string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsDbGlobal.GetDtFromSPNoReturnValue(spName, arrInParamsNames, arrInParamsValues);
        }

        public static DataSet FillDS
                   (string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsDbGlobal.GetDsFromSPNoReturnValue(spName, arrInParamsNames, arrInParamsValues);
        }

        public static DataSet FillDS
                   (string spName, string[] arrInParamsNames, string[] arrInParamsValues, SqlDbType[] arrInParamsTypes)
        {
            return clsDbGlobal.GetDsFromSPNoReturnValue(spName, arrInParamsNames, arrInParamsValues, arrInParamsTypes);
        }

        public static DataSet FillDS(string spName)
        {
            return clsDbGlobal.GetDsFromSPNoParamsNoReturnValue(spName);
        }
    
    

        public static DataTable GetDtFromSPNoReturnValueIncTypes
                  (string spName, string[] arrInParamsNames, string[] arrInParamsValues, SqlDbType[] arrInParamsTypes)
        {
            return clsDbGlobal.GetDtFromSPNoReturnValueIncTypes(spName, arrInParamsNames, arrInParamsValues, arrInParamsTypes);
        }

       

        public static DataTable FillDtNoParams (string spName)
        {
            return clsDbGlobal.GetDtFromSPNoParamsNoReturnValue(spName);
        }
        //return true or false
        public static bool ExecuteSQL
                        (string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsDbGlobal.runSp_NoReturn(spName, arrInParamsNames, arrInParamsValues);
        }
        //return "" if no value or value
        public static string ExecuteSQLScalar
                        (string spName, string[] arrInParamsNames, string[] arrInParamsValues)
        {
            return clsDbGlobal.runSp_ScalarQuery_NoReturn(spName, arrInParamsNames, arrInParamsValues);
        }

        public static Boolean ExecuteSQLNoParams
                        (string spName)
        {
            return clsDbGlobal.runSp_NonQuery_NoParams_NoReturn(spName);
        }

        public static string ExecuteSQLScalar
                       (string spName, string[] arrInParamsNames, string[] arrInParamsValues, SqlDbType[] arrInParamsTypes)
        {
            return clsDbGlobal.runSp_ScalarQuery_NoReturn(spName, arrInParamsNames, arrInParamsValues, arrInParamsTypes);
        }


        public static string addSqlParamWithComma_EmptyAsZero(string strValue,Boolean blnLast)
        {
            strValue = strAntiXss(strValue);
            if (blnLast)
            {
                if (strValue.Trim() == "")
                    return "0";
                else
                    return strValue;
            }
            else
            {
                if (strValue.Trim() == "")
                    return "0,";
                else
                    return strValue + ",";
            }
        }


        public static string convertNullToEmpty(object strValue)
        {
            if (strValue == DBNull.Value)
            {
                return "";
            }
            else
            {
                //string strValue2 = strAntiXss(strValue.ToString());
                return strValue.ToString();
            }

        }

        public static Boolean convertBooleanNullToFalse(object strValue)
        {
            if (strValue == DBNull.Value || Convert.ToBoolean(strValue) == false)
            {
                return false;
            }
            else
            {
                return true;
            }

        }


        public static string convertDateNullToEmpty(object strValue)
        {
            if (strValue == DBNull.Value)
            {
                return "";
            }
            else
            {
                return string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(strValue));
            }
        }


        public static string convertEmptyStringToNull(string strValue)
        {
            if (strValue == "")
            {
                return "null";
            }
            else
            {
                return strValue;
            }
        }


        public static string convertBooleanToYesOrNo(object strValue)
        {
            if (Convert.ToBoolean(strValue))
            {
                return "��";
            }
            else
            {
                return "��";
            }

        }

        public static Boolean isMatchRegx(string str)
        {
            return Regex.IsMatch(str, strRegx, RegexOptions.Compiled);
        } 
        
        //*********************************************  anti xss to a string *************************
        public static string strAntiXss(string strValue)
        {
            if (!Regex.IsMatch(strValue, strRegx))
            {
                writeLog("regx failed for allowed chars, value=" + strValue,true);
                return "";
            }
            strValue = strValue.Replace("'", "''");
            return AntiXss.HtmlAttributeEncode(strValue);
        }
        //*********************************************  anti xss to a string *************************



        public static Boolean convert_str_true_ToBooleanTrue_elseBooleanFalse(object strValue)
        {
            if (strValue.ToString().ToLower() == "true")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Boolean convert_str_notEmpty_ToBooleanTrue_elseBooleanFalse(object strValue)
        {
            if (strValue.ToString().Trim() != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Boolean convertBoolean_OneToTrue_OthersToFalse(object strValue)
        {
            if (strValue.ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        
        
        public static string urlAntiXss(string strValue)
        {
            return AntiXss.UrlEncode(strValue);
        }



        public static string replaceGereshEncoded(string strValue)
        {
            string temp = strValue.Replace("&#39;&#39;", "&#39;");
            //strValue = temp.Replace("''", "'");//.Replace("&#39;&#39;", "&#39;");
            return temp;
        }


        public static string htmlDecode(string strValue)
        {

            string temp = HttpUtility.HtmlDecode(strValue);
            //do it twice in case special chars like ampersand converted also to "&amp;"
            temp = HttpUtility.HtmlDecode(temp);
                //HttpContext.Current.Server.HtmlDecode(strValue);
            return temp;
        }

        public static string urlDecode(string strValue)
        {
            string temp = HttpUtility.UrlDecode(strValue);
            //`HttpContext.Current.Server.HtmlDecode(strValue);
            return temp;
        }

        public static string addSqlParamWithComma_EmptyAsIs(string strValue, Boolean blnLast)
        {
            if (blnLast)
            {
                return strValue;
            }
            else
            {
                return strValue + "�!�";
            }
        }

        public static string addSql_IntParamWithComma_EmptyAsIs(int strValue, Boolean blnLast)
        {
            if (blnLast)
            {
                return strValue.ToString();
            }
            else
            {
                return strValue.ToString() + "�!�";
            }
        }

        public static string addSqlParamWithComma_EmptyAsNull(string strValue, Boolean blnLast)
        {
            strValue = strAntiXss(strValue);
            if (blnLast)
            {
                if (strValue.Trim() == "")
                    return "null";
                else
                    return strValue;
            }
            else
            {
                if (strValue.Trim() == "")
                    return "null,";
                else
                    return strValue + ",";
            }
        }

        /// <summary>
        /// empty means the param value not found by this code
        /// </summary>
        /// <param name="paramCode"></param>
        /// <returns></returns>
        public static string getParamValueByCode(string paramCode)
        {
            string strInParamsNames = "paramCode";
            string strInParamsValues = paramCode;
            string[] arrInParamsNames = strInParamsNames.Split(',');
            string[] arrInParamsValues = strInParamsValues.Split(',');
            return clsDbGlobal.runSp_ScalarQuery_NoReturn("[dbo].[getParamValueByCode]", arrInParamsNames, arrInParamsValues);
        }


        public static DataSet getMdTables()
        {
            return clsDbGlobal.GetDsFromSPNoParamsNoReturnValue("[dbo].[mdGetAll]");
        }


        public static void BatchBulkCopy(DataTable dataTable, string DestinationTbl, int batchSize)
        {
            clsDbGlobal.BatchBulkCopy(dataTable, DestinationTbl, batchSize);
        }
        #region helperFunction
        public static void writeLog(string msg, Boolean isErr)
        {
            string sSource = "PikudBL";
            //string sLog = "PikudBL";

            //if (!EventLog.SourceExists(sSource))
            //    EventLog.CreateEventSource(sSource, sLog);

            if (isErr)
            {
                EventLog.WriteEntry(sSource, " BL layer error found: \n\n" + msg
                                            , EventLogEntryType.Error, 991);
            }
            else
            {
                EventLog.WriteEntry(sSource, " BL layer info msg: \n\n" + msg
                                            , EventLogEntryType.Information, 992);
            }
        }
        #endregion

        
    }
}
